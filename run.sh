#!/bin/bash

root_directory=$(eval echo ~${SUDO_USER})
config_json_filename="$root_directory/.podfox.json"
path_filename="$root_directory/Music/podcast"

create_config_file()
{
    if [[ ! -f "$config_json_filename" ]]
    then
        printf "{\n    \"podcast-directory\" : \""%s"\",\n    \"maxnum\"            : 5\n}" "$path_filename" > "$config_json_filename"
    fi

    if [[ ! -d "path_filename" ]]
    then
        mkdir -p "$path_filename"
    fi
}

check_time()
{
    local hour=$(date +%H)
    local int_hour=$(echo "$hour" | sed 's/^0*//')

    if [[ "$int_hour" -lt 10 ]]
    then
        echo "in the morning"
    elif [[ "$int_hour" -lt 15 ]]
    then
        echo "at noon"
    elif [[ "int_hour" -lt 19 ]]
    then
        echo "in the afternoon"
    else
        echo "at night"
    fi
}

play_audio_file()
{
    local audio_file_type=( "*.mp3" "*.ogg" "*.wav" "*.m4a" )
    local audio_file_string=""
    local audio_file_array=()
    local audio_file

    for audio_file in ${audio_file_type[@]}
    do
        audio_file_string+=$(find "$1" -name "$audio_file")
        audio_file_string+=" "
    done

    audio_file_array=($audio_file_string)
    audio_file_array=( $(shuf -e "${audio_file_array[@]}") )

    # for audio_file in ${audio_file_array[@]}
    # do
    #     mplayer "$audio_file"
    # done

    for ((i=0;i<${#audio_file_array[@]};i++))
    do
        if [ $i -lt $2 ]
        then
            mplayer "${audio_file_array[i]}"
        fi
    done
}

remove_audio_file()
{
    local audio_file_type=( "*.mp3" "*.ogg" "*.wav" "*.m4a" )
    local audio_file

    for audio_file in ${audio_file_type[@]}
    do
        find "$path_filename" -name "$audio_file" -exec rm -fr {} \;
    done
}

download_podcast()
{
    local podcast_url_list=(
        "http://www.ximalaya.com/album/257813.xml"
        "http://www.ximalaya.com/album/9329526.xml"
        "http://www.ximalaya.com/album/14740113.xml"
        "http://www.ximalaya.com/album/234193.xml"
        "http://www.ximalaya.com/album/47050181.xml"
        "http://www.ximalaya.com/album/260744.xml"
        "http://www.ximalaya.com/album/3258094.xml"
        "http://www.ximalaya.com/album/34875808.xml"
        "https://open.firstory.me/rss/user/ckgvv1m2ah8re0903njm7tcun"
        "https://open.firstory.me/rss/user/ckesn2sbvx5060839umop16go"
        "https://api.soundon.fm/v2/podcasts/38cf0c12-46f7-4012-bcfb-34d85c98ab77/feed.xml"
        "https://api.soundon.fm/v2/podcasts/0cb16276-249c-4d9d-834a-bbbaf7a51cc7/feed.xml"
        "https://api.soundon.fm/v2/podcasts/51ed4193-b0f5-4cab-a74a-e2de7533644a/feed.xml"
        "https://feeds.soundon.fm/podcasts/32aafa77-19a8-41dd-9aa4-62358554de91.xml"
        "https://feeds.soundon.fm/podcasts/0c0ee818-7e18-48cb-b2b9-ca61b2796e3a.xml"
        "https://feeds.soundon.fm/podcasts/e3fdde8c-2da3-4986-836c-a8a0e04051ed.xml"
        "https://feeds.soundon.fm/podcasts/5360fdae-1b91-480e-bb73-6f250ba35927.xml"
        "https://feeds.soundon.fm/podcasts/58503fd8-d3d8-457d-97de-60e9786bc77e.xml"
        "https://feeds.soundon.fm/podcasts/e59cea95-e4d0-4630-9c85-33397f0c6512.xml"
        "http://rss.lizhi.fm/rss/30562.xml"
        "https://news.un.org/feed/subscribe/zh/audio-product/all/audio-rss.xml"
    )
    local i

    for i in $(seq 1 ${#podcast_url_list[@]})
    do
        python3 /home/pi/work/git/pi-stories-teller/src/main.py import "${podcast_url_list[i-1]}" "podcast_00$i"
        python3 /home/pi/work/git/pi-stories-teller/src/main.py update "podcast_00$i"
    done

    python3 /home/pi/work/git/pi-stories-teller/src/main.py download --how-many=1
}

create_task()
{
    local cur_time=$(check_time)

    if [ "$1" == "--download" ]
    then
        echo "Download only"
        download_podcast
        exit
    fi

    play_audio_file "$path_filename" 100
    # mplayer "$root_directory/Music/playlist/happy_birthday_30years.mp3"

    if [ "$cur_time" == "at night" ]
    then
        remove_audio_file
        download_podcast
        play_audio_file "$root_directory/Music/playlist" 3
    fi
}

create_config_file
create_task $@
